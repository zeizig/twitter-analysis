FROM python:3.6

RUN apt-get update && \
    apt-get upgrade -y

# Use older versions since current pipenv has problems parsing package names
# with "." in them. This includes `dogpile.cache` used by OpenStack
RUN pip install pip==18.0 && pip install pipenv==2018.7.1

COPY .env /app/.env
COPY ./Pipfile /app/Pipfile
COPY ./Pipfile.lock /app/Pipfile.lock

WORKDIR /app

# Install packages to the system, since we don't need virtualenvs inside
# containers
RUN cd /app && pipenv install --system --deploy --ignore-pipfile

CMD gunicorn -b 0.0.0.0:5000 twitter_analysis.wsgi:app
