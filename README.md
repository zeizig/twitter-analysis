# Twitter Analysis

## Requirements

* Docker
* `docker-compose`
* Data files in the `data` directory

## Running the application

```bash
docker-compose up
```
