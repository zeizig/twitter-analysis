import os
from pathlib import Path

from flask import Flask
from dotenv import load_dotenv


def create_app():
    # Load environment variables from .env file
    env_path = Path('..') / '.env'
    load_dotenv(dotenv_path=env_path)

    # Create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # Load the instance config from environment variables
    app.config.from_object('twitter_analysis.settings.base')

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .analysis import (
        views as analysis_views,
        api_views as analysis_api_views,
    )
    app.register_blueprint(analysis_views.bp)
    app.register_blueprint(analysis_api_views.bp)

    return app
