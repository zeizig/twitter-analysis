from flask import Blueprint, url_for, jsonify

from .tasks import analyze_tweets as analyze_tweets_task

bp = Blueprint('analysis_api', __name__, url_prefix='/api')


@bp.route('/analyze-tweets', methods=['POST'])
def analyze_tweets():
    """ Start analysing tweets.
    """
    task = analyze_tweets_task.delay()
    status_url = url_for('analysis_api.status_check', task_id=task.id)
    return jsonify({'status_url': status_url}), 202, {'Location': status_url}


@bp.route('/tasks/<task_id>')
def status_check(task_id: int):
    """ Check the progress of analysing tweets.
    """
    task = analyze_tweets_task.AsyncResult(task_id)
    response = {
        'state': task.state,
    }

    if task.state == 'PENDING':
        # Job did not start yet
        response.update({
            'status': 'Pending...',
        })
    elif task.state != 'FAILURE':
        response.update({
            'status': task.info.get('status', ''),
        })
        response.update(task.info)
    else:
        response.update({
            'status': str(task.info),  # Exception raised
        })

    return jsonify(response)
