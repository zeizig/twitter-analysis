import json
import re
from string import punctuation

from twitter_analysis.celery import app as celery
import twitter_analysis.settings.base as settings


KEYWORDS = ['han', 'hon', 'den', 'det', 'denna', 'denne', 'hen']


@celery.task(bind=True)
def analyze_tweets(self):
    """ Analyze the tweets' data in the DATA_PATH folder.
    """
    total_count = 0
    counts = {}
    for keyword in KEYWORDS:
        counts[keyword] = 0

    # Do not include .gitignore
    files = settings.DATA_PATH.glob('*-*')
    file_names = list(files)
    nr_of_files = len(file_names)
    for index, file_name in enumerate(file_names):
        with open(file_name) as file:
            for line in file:

                line = line.strip()
                if not line:
                    # Empty line
                    continue

                tweet = json.loads(line)
                if 'retweeted_status' in tweet:
                    # Is a retweet
                    continue

                total_count += 1

                punctuation_regex = re.compile(rf'[{punctuation}]')
                words = punctuation_regex.sub(' ', tweet['text'])
                for keyword in KEYWORDS:
                    counts[keyword] += words.count(keyword)

            self.update_state(
                state='PROGRESS',
                meta={
                    'current': index,
                    'total': nr_of_files,
                    'status': 'In progress',
                },
            )

    return {
        'status': 'Completed!',
        'result': {'total_count': total_count, 'counts': counts},
    }
