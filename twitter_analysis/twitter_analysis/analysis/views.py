from flask import Blueprint, render_template

bp = Blueprint('analysis', __name__)


@bp.route('/', methods=('GET',))
def index():
    """ Index route.
    """
    return render_template('home.html')
