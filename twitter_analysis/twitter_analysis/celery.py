from celery import Celery


app = Celery('twitter_analysis', include=('twitter_analysis.analysis.tasks',))
app.config_from_object('twitter_analysis.celeryconfig')
