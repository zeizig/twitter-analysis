import twitter_analysis.settings.base as settings

broker_url = f'pyamqp://{settings.RABBITMQ_USER}:{settings.RABBITMQ_PASS}@{settings.RABBITMQ_HOST}:{settings.RABBITMQ_PORT}'
result_backend = 'rpc://'
