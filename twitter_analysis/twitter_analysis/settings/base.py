import os
from pathlib import Path
from dotenv import load_dotenv

load_dotenv(dotenv_path=(Path('..') / '.env'))

RABBITMQ_USER = os.getenv('RABBITMQ_USER')
RABBITMQ_PASS = os.getenv('RABBITMQ_PASS')
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT')

DATA_PATH = Path('..') / 'data'
