(() => {
  const ANALYZE_TWEETS = '/api/analyze-tweets';
  const POLL_TIME = 500;

  let statusUrl = null;

  const button = document.querySelector('#analyze-button');
  const chartCanvas = document.querySelector('#chart');
  const progressBar = document.querySelector('#progress-bar');

  const updateProgressBar = percentage => {
    progressBar.style.width = `${percentage}%`;
    progressBar.innerHTML = `${percentage.toFixed(0)}%`;
    if (percentage > 0) {
      progressBar.classList.remove('progress__bar--hidden');
    }
  };

  const drawChart = result => {
    const { total_count: total, counts } = result;
    const labels = Object.keys(counts);
    const values = Object.values(counts)
      .map(count => (count / total * 100).toFixed(2));

    const ctx = chartCanvas.getContext('2d');
    new Chart(ctx, {
      type: 'bar',

      data: {
        labels,
        datasets: [{
          label: 'Pronouns per 100 tweets',
          data: values,
          backgroundColor: 'rgba(128, 147, 241, 0.6)',
          borderColor: 'rgba(128, 147, 241, 1)',
          borderWidth: 1,
        }],
      },

      options: {
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{
            type: 'linear',
            scaleLabel: {
              labelString: 'Nr. of pronouns per 100 tweets',
              display: true,
            },
          }],
          xAxes: [{
            type: 'category',
            scaleLabel: {
              labelString: 'Pronoun',
              display: true,
            },
          }],
        },
      },
    });
  };

  const handleReceiveStatus = response => {
    const { state } = response;

    if (state === 'SUCCESS') {
      button.innerHTML = 'Analyze tweets';
      button.addEventListener('click', handleAnalyzeButtonClicked);
      statusUrl = null;
      updateProgressBar(100);

      drawChart(response.result);
    } else if (state === 'PENDING') {
      setTimeout(pollStatus, POLL_TIME);
    } else if (state === 'PROGRESS') {
      const {total, current} = response;
      const width = current * 100 / total;

      updateProgressBar(width);

      setTimeout(pollStatus, POLL_TIME);
    } else {
      button.innerHTML = 'ERROR! Try again';
      button.addEventListener('click', handleAnalyzeButtonClicked);
    }
  };

  const pollStatus = () => {
    fetch(statusUrl, {
      headers: {
        'Accept': 'application/json',
      },
    })
      .then(res => res.json())
      .then(handleReceiveStatus);
  };

  const handleAnalyzeButtonClicked = () => {
    button.removeEventListener('click', handleAnalyzeButtonClicked);
    button.innerHTML = 'Loading...';

    fetch(ANALYZE_TWEETS, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
    })
      .then(res => res.json())
      .then(res => {
        statusUrl = res.status_url;
        pollStatus();
      });
  };

  const bindEventListeners = () => {
    button.addEventListener('click', handleAnalyzeButtonClicked);
  };

  document.addEventListener('DOMContentLoaded', () => {
    bindEventListeners();
  });
})();
